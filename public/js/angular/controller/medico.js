
//alert("aquii");
(function(){
    angular.module('app.medico',[]).config(function(hljsServiceProvider,$interpolateProvider,$routeProvider,$locationProvider) {
        hljsServiceProvider.setOptions({
        // replace tab with 4 spaces
        tabReplace: '    '
    });
        
    $interpolateProvider.startSymbol('{{{');
    $interpolateProvider.endSymbol('}}}');


    //$locationProvider.html5Mode(true);
    $routeProvider.
    when('/medico', {
        templateUrl: '/template/create_medico.html',
        controller: 'GuardarMedicoController'
    }).when('/create_medico', {
        templateUrl: '/template/create_medico.html',
        controller: 'miController'
    }).when('/listado_medico', {
        templateUrl: '/template/listado_medico.html',
        controller: 'listadoMedico'
    }).when('/edita_user/:id', {
        templateUrl: '/template/editar_usuario.html',
        controller: 'editarUser'
    }).when('/view_user/:id', {
        templateUrl: '/template/view_user.html',
        controller: 'editarUser'
    }).otherwise({
        redirectTo: '/'
    })

    }).controller('editarUser',function($scope,$http,$routeParams){

        var id=$routeParams.id;
        //console.log(id);
        $http({
            'url':'/medico/medico/'+id,
            'data':{id:id},
            'method':'get',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(datos){
            console.log(datos);
            //console.log(data);
            //$scope.nombre="dddd";
            $scope.dataForm=datos.dataForm;
            $scope.listaEstado=datos.estado;
            $scope.listaGrupo=datos.grupo;
            console.log($scope.dataForm);
        }).error(function(error){
            console.log(error);
        })
 $scope.$watch('dataForm.correo',function(nuevo,anterior){
            if(!nuevo) return;
            if(!/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/.test(nuevo)){
                $scope.mostrarCorreo="El correo es invalido";
            }else{
                $scope.mostrarCorreo=false;
            }
        });

        $scope.$watch('dataForm.apellido',function(nuevo,anterio){
            if(!nuevo) return;
            if(!/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nuevo)){
                $scope.mostrarApellido="El apellido no debe contener numeros ni caracteres especiales";
            }else if(nuevo.length<=3){
            $scope.mostrarApellido="El apellido debe contener al menos 4 letras";
            }else{
                $scope.mostrarApellido=false;
            }
        });

        $scope.$watchGroup(['dataForm.password','dataForm.repetir_clave'],function(nuevo,anterio){
            if(!nuevo[0] || !nuevo[1]) return;
            if(nuevo[0]!==nuevo[1]){
                $scope.mostrarRepetirClave="Las Claves no coinciden";
            }else{
                $scope.mostrarRepetirClave=false;
            }
        });

        $scope.$watch('dataForm.password',function(nuevo,anterior){
            console.log(nuevo);
            if(nuevo!=''){
            $scope.mostrarClave=false;
            }
        });

        $scope.$watch('dataForm.nombre',function(nuevo,anterio){
            if(!nuevo) return;
            if(!/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nuevo)){
                $scope.mostrarNombre="El nombre no debe contener numeros ni caracteres especiales";
            }else if(nuevo.length<=3){
            $scope.mostrarNombre="El nombre debe contener al menos 4 letras";
            }else{
                $scope.mostrarNombre=false;
            }
        });

        

        $scope.$watch('dataForm.cedula',function(nuevo,anterior){
            if(!nuevo) return;
            if(/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nuevo)){
                $scope.mostrarCedula="La cedula debe ser solo numerica";
            }else{
                $scope.mostrarCedula=false;
            }
        });

        $scope.$watch('dataForm.telefono_local',function(nuevo,anterior){
            if(!nuevo) return;
            if(/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nuevo)){
                $scope.mostrarTelefonoFijo="El telefono local debe ser solo numerica";
            }else{
                $scope.mostrarTelefonoFijo=false;
            }
        });
        $scope.$watch('dataForm.telefono_celular',function(nuevo,anterior){
            if(!nuevo) return;
            if(/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nuevo)){
                $scope.mostrarTelefonoCelular="El telefono celular debe ser solo numerica";
            }else{
                $scope.mostrarTelefonoCelular=false;
            }
        });

       $scope.editarUsuario = function () {
                    console.log($scope.dataForm);
                angular.element('#btn-consultar')
                    .html('<i class="fa fa-spinner fa-spin fa-lg fa-fw"></i> Consultado...')
                    .prop('disabled', true);
                $http({
                    method: 'PUT',
                    url: '/admin/usuario/'+$scope.dataForm.id,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data:{
                        'form': $scope.dataForm
                    }
                }).success(function(data){
                        console.log(data['error']);
                        if(data.statusCode.mensaje!=false){
                            $scope.mostrarMensajeExito=data.statusCode.mensaje;
                            console.log($scope.mostrarMensajeExito);
                        }else{
                            $scope.mostrarNombre=data.error.name;
                            $scope.mostrarCorreo=data.error.correo;
                            $scope.mostrarApellido=data.error.apellido;
                            $scope.mostrarCedula=data.error.cedula;
                            $scope.mostrarEstado=data.error.estado;
                            $scope.mostrarUsuario=data.error.usuario;
                            $scope.mostrarEstado=data.error.estado;
                            $scope.mostrarTelefonoCelular=data.error.telefono_celular;
                            $scope.mostrarTelefonoFijo=data.error.telefono_fijo;
                            $scope.mostrarGrupo=data.error.grupo;
                            $scope.mostrarEstatus=data.error.estatus;
                            $scope.mostrarClave=data.error.clave;
                            $scope.mostrarRepetirClave=data.error.repetir_clave;

                        }
                            //$window.location.href = '/admin/usuario/create';
                        }).error(function(){
                 angular.element('#btn-consultar')
                            .html('<i class="fa fa-search"></i> Consultar')
                            .prop('disabled', false);

                        //Recargamos el recaptcha
                        //$scope.reaload_widget($scope.widgetId);
                });
                }

    }).controller('miController',function($scope,$http){
        $http({
            'url':'/medico/medico/create',
            'data':{},
            'method':'get',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data){
            $scope.listaEstado=data.estado;
        }).error(function(error){
            console.log(error);
        })

    }).controller('listadoMedico',function($scope, DTOptionsBuilder, DTColumnBuilder,DTDefaultOptions,$http,$modal){
    $scope.showConfirm = function(id,estatus) {
    $modal.showModal = true;
    // Appending dialog to document.body to cover sidenav in docs app
    var modalInstance=$modal.open(
        {

        templateUrl:(estatus=='A')?'/template/delete_user.html':'/template/reactivar_user.html',
        controller: 'deleteUser',
        size: 'lg',
        resolve: {
            id: function () {return id;},
            estatus: function () {return estatus;}
    }

    });
  };

       //$scope.nombreM="Rida";

        $http({
            'url':'/medico/medico',
            'data':{},
            'dataType': "json",
            'method':'get',
             headers: {'Content-Type': 'application/x-www-form-urlencoded'}


        }).success(function(data){
            $scope.dtInstance={};
            //$scope.persons = $resource('data.json').query();
    $scope.listado = DTOptionsBuilder.fromSource(data.listado);
    $scope.dtOptions = DTOptionsBuilder.newOptions('aaSorting', [3, 'desc']).withPaginationType('full_numbers')
    //.withDOM('<div> holaaaaa </div>')
    .withLanguage({
    "sEmptyTable":     "No hay datos para cargar en la tabla",
    "sInfo":           "Mostrando _START_ de _END_ de _TOTAL_ entradas",
    "sInfoEmpty":      "Mostrando 0 de 0 de 0 entradas",
    "sInfoFiltered":   "(filtradas _MAX_ entradas totales)",
    "sInfoPostFix":    "",
    "sInfoThousands":  ",",
    "sLengthMenu":     "Mostrar _MENU_ entradas",
    "sLoadingRecords": "Cargando...",
    "sProcessing":     "Procesando...",
    "sSearch":         "Buscar:",
    "sZeroRecords":    "No se encontraron registros",
    "sColvis": "C<'clear'>lfrtip",
    "oPaginate": {
        "sFirst":    "Primera",
        "sLast":     "Última",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": activar para ordenar de forma ascendente",
        "sSortDescending": ": activar para ordenar de forma descendente"
    },
    "buttons": {
        exportOptions: {
            columns: ':visible'
        },
               print: 'Imprimir',
  }
  })

.withButtons([
           {
        extend: "copy",
        text:"Copiar",
        exportOptions: {
            columns: ':visible'
        },
        exportData: {decodeEntities:true}
    },
    {
        extend: "excel",
        filename:  "Data_Analysis",
        title:"Data Analysis Report",
        exportOptions: {
            columns: ':visible'
        },
        //CharSet: "utf8",
        exportData: { decodeEntities: true }
    },
    {
        extend: "csv",
        exportOptions: {
            columns: ':visible'
        },
        exportData: {decodeEntities:true}
    },


    {
        extend: "pdf",
        fileName:  "Data_Analysis",
        title:"Listado de Usuario",
        orientation: 'landscape',
         message: 'Usuario registrados en el sistema',
        customize: function (doc) {

                    doc.content.splice(1, 0, [{
                        margin: [0, 0, 0, 12],
                        width: 250,
                        message: 'Usuario registrados en el sistema',
                        alignment: 'center',
                        image: 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSZ9vXvMTrz5QAXziamO3piMhSm-o27jbpTEWFfsWBbIfnjAzuqpQ',
                        }
                        ])
                    },
        exportOptions: {
            columns: ':visible',
            customize: ':visible'
        },
        exportData: {decodeEntities:true}
    },

    {
        extend: 'print',
        //text: 'Print current page',
        autoPrint: false,
        exportOptions: {
            columns: ':visible'
        }
    },
    {
    text:"Columna Visible",
    extend:'colvis'
    }

]).withBootstrap();

  /*.withButtons([
        'print',
        'excel',
        'pdfFlash',
        'copy',
        'colvis',
  ]).
  withBootstrap();*/
  console.log($scope.listado);
    /*$scope.dtOvtions = {
    //------------------------------------------------------------------
    // Get from ajax source
    //------------------------------------------------------------------
    autoWidth: false,
    paging: true,
    pageLength: 25,
    pagingType: "full_numbers",
    language: {
    "info":  "Mostrando _START_ de _END_ de _TOTAL_ entradas",
    "emptyTable":  "No hay datos para cargar en la tabla",
    "zeroRecords":    "No se encontraron registros",  // Set up pagination text
      paginate: {
        first: "Primero",
        last: "Anterior",
        next: "Siguiente",
        previous: "Ultimo"
      },
      search: "Buscar: ",
      lengthMenu: "_MENU_ Recorrer Pagina"
    }
  };*/
        // Active Buttons extension
    //.withTableTools('../../../vendor/datatables-tabletools/swf/copy_csv_xls_pdf.swf')

    //DTOptionsBuilder


        //.withPaginationType('full_numbers');
            //console.log(data['listado']);
            //$scope.listado=data['listado'];
            //console.log($scope.listado);

        }).error(function(error){
            console.log(error);

        });
    //console.log(listado);
    //Funcion para eliminar usuario

       
}).controller('deleteUser', function ($scope,id,estatus,$modalInstance,$http,$window,$timeout){
    var modalInstance=$scope.$modalInstance;
    $scope.cancel = function () {
     $modalInstance.close();
};
  $scope.ok = function(){
    $http({
            'url':'/admin/usuario/'+id+"-"+estatus,
            'data':{id:id,estatus:estatus},
            'method':'delete',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(datos){
            console.log(datos);
            $scope.mostrarMensajeExito=datos.statusCode.mensaje;
            $timeout(function(){
                $modalInstance.close();
                $window.location.href='#listado_user';
            },800);
            //$scope.listado=datos['usuario'];
            //alert("aquiii");
            //$window.location.href='/admin/usuario/create#/buscar_paciente';
            
            
            //$window.location.reload();
        }).error(function(error){
            console.log(error);
        })
};
}).controller('GuardarMedicoController',function($scope,$http,$window){
    $scope.nombre="holaaa";
        console.log($scope.nombre);
        $scope.$watch('dataForm.correo',function(nuevo,anterior){
            if(!nuevo) return;
            if(!/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/.test(nuevo)){
                $scope.mostrarCorreo="El correo es invalido";
            }else{
                $scope.mostrarCorreo=false;
            }
        });

        $scope.$watch('dataForm.apellido',function(nuevo,anterio){
            if(!nuevo) return;
            if(!/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nuevo)){
                $scope.mostrarApellido="El apellido no debe contener numeros ni caracteres especiales";
            }else if(nuevo.length<=3){
            $scope.mostrarApellido="El apellido debe contener al menos 4 letras";
            }else{
                $scope.mostrarApellido=false;
            }
        });

        $scope.$watchGroup(['dataForm.password','dataForm.repetir_clave'],function(nuevo,anterio){
            if(!nuevo[0] || !nuevo[1]) return;
            if(nuevo[0]!==nuevo[1]){
                $scope.mostrarRepetirClave="Las Claves no coinciden";
            }else{
                $scope.mostrarRepetirClave=false;
            }
        });

        $scope.$watch('dataForm.nombre',function(nuevo,anterio){
            if(!nuevo) return;
            if(!/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nuevo)){
                $scope.mostrarNombre="El nombre no debe contener numeros ni caracteres especiales";
            }else if(nuevo.length<=3){
            $scope.mostrarNombre="El nombre debe contener al menos 4 letras";
            }else{
                $scope.mostrarNombre=false;
            }
        });

        

        $scope.$watch('dataForm.cedula',function(nuevo,anterior){
            if(!nuevo) return;
            if(/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nuevo)){
                $scope.mostrarCedula="La cedula debe ser solo numerica";
            }else{
                $scope.mostrarCedula=false;
            }
        });

        $scope.$watch('dataForm.telefono_local',function(nuevo,anterior){
            if(!nuevo) return;
            if(/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nuevo)){
                $scope.mostrarTelefonoFijo="El telefono local debe ser solo numerica";
            }else{
                $scope.mostrarTelefonoFijo=false;
            }
        });
        $scope.$watch('dataForm.telefono_celular',function(nuevo,anterior){
            if(!nuevo) return;
            if(/^[A-Za-z\_\-\.\s\xF1\xD1]+$/.test(nuevo)){
                $scope.mostrarTelefonoCelular="El telefono celular debe ser solo numerica";
            }else{
                $scope.mostrarTelefonoCelular=false;
            }
        });
       $scope.response_captcha = null;
       $scope.mensaje="hola";
        //Variable que mantiene el id del widget que crea google recaptcha
        $scope.widgetId = null;
        //Variable que mantiene el key para conectarse con google recaptcha
        $scope.key_captcha = '6LcVGSETAAAAANpezdNiI8Ll70uLkQnU-E3xwrtA';
        //Variable que almacenas los datos enviados desde el FRONT
        $scope.dataForm = {};
        //Funcion para validar la cedula introducida por el usuario
        var validar_usuario = function (_value_usuario, _value_clave) {
            //Verificamos que la varibale haya sido declarada en el scope
            if (typeof _value_usuario === 'undefined') {
                angular.element('#contenedor-mensajes').html('<div class="alert alert-warning">' +
                    '<div class="alert-link">' +
                    '<i class="fa fa-exclamation-circle fa-lg fa-fw"></i>' + "El campo usuario no puede quedar en blanco" + '</div>' +
                    '</div>');
                return false;
            }
            if (typeof _value_clave === 'undefined') {
                angular.element('#contenedor-mensajes').html('<div class="alert alert-warning">' +
                    '<div class="alert-link">' +
                    '<i class="fa fa-exclamation-circle fa-lg fa-fw"></i>' + "El campo clave no puede quedar en blanco" + '</div>' +
                    '</div>');
                return false;
            }

            return true;
        };

         $scope.guardarMedico = function () {
         	//alert("aquiii");
                angular.element('#btn-consultar')
                    .html('<i class="fa fa-spinner fa-spin fa-lg fa-fw"></i> Consultado...');
                $http({
                    method: 'POST',
                    url: '/medico/medico',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data:{
                        'form': $scope.dataForm
                    }
                }).success(function(data){
                        console.log(data['error']);
                        $scope.clase=data.clase;
                        if(data.statusCode.mensaje!=false){
                            $scope.mostrarMensajeExito=data.statusCode.mensaje;
                            console.log($scope.mostrarMensajeExito);
                             $scope.mostrarNombre='';
                            $scope.mostrarCorreo='';
                            $scope.mostrarApellido='';
                            $scope.mostrarCedula='';
                            $scope.mostrarEstado='';
                            $scope.mostrarEstado='';
                            $scope.mostrarTelefonoCelular='';
                            $scope.mostrarTelefonoFijo='';
                            $scope.mostrarGrupo='';
                            $scope.mostrarEstatus='';
                        }else{
                            $scope.mostrarNombre=data.error.name;
                            $scope.mostrarCorreo=data.error.correo;
                            $scope.mostrarApellido=data.error.apellido;
                            $scope.mostrarCedula=data.error.cedula;
                            $scope.mostrarEstado=data.error.estado;
                            $scope.mostrarEstado=data.error.estado;
                            $scope.mostrarTelefono=data.error.telefono;
                            $scope.mostrarGrupo=data.error.grupo;
                            $scope.mostrarEstatus=data.error.estatus;

                        }
                            //$window.location.href = '/admin/usuario/create';
                        }).error(function(){
                            console.log("aquii");
                 angular.element('#btn-consultar')
                            .html('<i class="fa fa-search"></i> Consultar')
                            .prop('disabled', false);

                        //Recargamos el recaptcha
                        //$scope.reaload_widget($scope.widgetId);
                });
                }



           $scope.editarUsuario = function () {
                angular.element('#btn-consultar')
                    .html('<i class="fa fa-spinner fa-spin fa-lg fa-fw"></i> Consultado...');
                $http({
                    method: 'POST',
                    url: '/admin/usuario',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data:{
                        'form': $scope.dataForm
                    }
                }).success(function(data){
                        console.log(data['error']);
                        $scope.clase=data.clase;
                        if(data.statusCode.mensaje!=false){
                            $scope.mostrarMensajeExito=data.statusCode.mensaje;
                            console.log($scope.mostrarMensajeExito);
                        }else{
                            $scope.mostrarNombre=data.error.name;
                            $scope.mostrarCorreo=data.error.correo;
                            $scope.mostrarApellido=data.error.apellido;
                            $scope.mostrarCedula=data.error.cedula;
                            $scope.mostrarEstado=data.error.estado;
                            //$scope.mostrarUsuario=data.error.usuario;
                            $scope.mostrarEstado=data.error.estado;
                            $scope.mostrarTelefonoCelular=data.error.telefono_celular;
                            $scope.mostrarTelefonoFijo=data.error.telefono_fijo;
                            $scope.mostrarGrupo=data.error.grupo;
                            $scope.mostrarEstatus=data.error.estatus;
                            $scope.mostrarClave=data.error.clave;
                            $scope.mostrarRepetirClave=data.error.repetir_clave;

                        }
                            //$window.location.href = '/admin/usuario/create';
                        }).error(function(){
                            console.log("aquii");
                 angular.element('#btn-consultar')
                            .html('<i class="fa fa-search"></i> Consultar')
                            .prop('disabled', false);

                        //Recargamos el recaptcha
                        //$scope.reaload_widget($scope.widgetId);
                });
                }





                });

                })();