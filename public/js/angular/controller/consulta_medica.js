angular.module('app.consultaPacientes', []).
config(function ($interpolateProvider,$routeProvider,$locationProvider){
	$interpolateProvider.startSymbol('{{{');
    $interpolateProvider.endSymbol('}}}');
	$routeProvider
	.when('/buscar_paciente',{
		'templateUrl':'/template/buscar_paciente.html',
		'controller':'consultaPacientes'
	}).when('/vistaPaciente/:id', {
		templateUrl: '/template/vista_paciente.html',
		controller: 'viewPaciente'
	}).otherwise({
		redirectTo: '/'
	})
	
}).controller('consultaPacientes', ['$scope','$http','$timeout', function ($scope,$http,$timeout) {
	mostrarPaciente=false;
	mostrarConsulta=false;
	$scope.busarPorCedula=function(){
		$http({
			'url':'/consulta/paciente',
			'data':$scope.dataForm,
			'method':'post',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(data){
			if(data.statusCode=='mostrarPaciente'){
				$scope.mostrarPaciente=true;
				$scope.mostrarConsulta=false;
				$scope.valorMensaje=false;
				$scope.listaEstado=data.estado;
				//console.log(data.estado);
			}else if(data.statusCode=='mostrarConsulta'){
				$scope.mostrarConsulta=true;
				$scope.valorMensaje=false;
				console.log($scope.mostrarConsulta);
				//console.log($scope.mostrarConsulta);
				$scope.mostrarPaciente=false;
				$scope.listado=data.listado;
				console.log(data.listado);
			}else if(data.statusCode=="errorCedulaMenor"){
				$scope.valorMensaje=true;
				$scope.mostrarMensaje=data.mensaje.mensaje;
				$scope.mostrarPaciente=false;
				$scope.mostrarConsulta=false;
				console.log(data.mensaje.mensaje);
			}else if(data.statusCode=="errorCedulaNumeric"){
				$scope.valorMensaje=true;
				$scope.mostrarMensaje=data.mensaje.mensaje;
				$scope.mostrarPaciente=false;
				$scope.mostrarConsulta=false;
			}

		}).error(function(error){
			console.log(error);
		});
	}

	$scope.guardarPaciente=function(){
		$http({
			url:'/consulta/paciente',
			data:{
			'dataForm':$scope.dataForm},
			method:'post',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(data){
			if(data.error!=''){
				$scope.mostrarNombre=data.error.nombre;
				$scope.mostrarApellido=data.error.apellido;
				$scope.mostrarTelefonoCelular=data.error.telefono_celular;
				$scope.mostrarCorreo=data.error.correo;
				$scope.mostrarEstado=data.error.estado_id;
				$scope.mostrarEstatus=data.error.estatus;
				$scope.mostrarDireccion=data.error.direccion;
			}else{
			$scope.mensaje=data.status.mensaje;
			$scope.clase=data.clase;
			$timeout(function(){
			$http({
			'url':'/consulta/paciente',
			'data':$scope.dataForm,
			'method':'post',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(data){
			if(data.statusCode=='mostrarPaciente'){
				$scope.mostrarPaciente=true;
				$scope.mostrarConsulta=false;
				$scope.valorMensaje=false;
				$scope.listaEstado=data.estado;
				//console.log(data.estado);
			}else if(data.statusCode=='mostrarConsulta'){
				$scope.mostrarConsulta=true;
				$scope.valorMensaje=false;
				console.log($scope.mostrarConsulta);
				//console.log($scope.mostrarConsulta);
				$scope.mostrarPaciente=false;
				$scope.listado=data.listado;
				console.log(data.listado);
			}else if(data.statusCode=="errorCedulaMenor"){
				$scope.valorMensaje=true;
				$scope.mostrarMensaje=data.mensaje.mensaje;
				$scope.mostrarPaciente=false;
				$scope.mostrarConsulta=false;
				console.log(data.mensaje.mensaje);
			}else if(data.statusCode=="errorCedulaNumeric"){
				$scope.valorMensaje=true;
				$scope.mostrarMensaje=data.mensaje.mensaje;
				$scope.mostrarPaciente=false;
				$scope.mostrarConsulta=false;
			}
		}).error(function(error){
			console.log(error);
		});
			},500);
		}
			//$window.location.href="#"
		}).error(function(error){
			console.log(error);
		});
	}
}]).controller('viewPaciente',['$scope','$http','$routeParams',function($scope,$http,$routeParams){
	var id=$routeParams.id;
	$http({
	'url':'/consulta/paciente/'+id,
	'data':{id:id},
	'method':'get',
     headers: {'Content-Type': 'application/x-www-form-urlencoded'}
}).success(function(data){
	//$scope.mostrarConsulta2="Holaaaaaa";
	if(data.dataForm.estatus==1){
		data.dataForm.estatus="Activo";
	}else{
		data.dataForm.estatus="Inactivo";
	}
	$scope.dataForm=data.dataForm;
	$scope.estado=data.estado;
}).error(function(error){
});



}]);