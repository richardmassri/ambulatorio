angular.module('app.consultaMedica',[]).
config(function ($interpolateProvider,$routeProvider,$locationProvider){
	$interpolateProvider.startSymbol('{{{');
    $interpolateProvider.endSymbol('}}}');
	$routeProvider
	.when('/consultaMedica/:id', {
		templateUrl: '/template/consultaMedicaPaciente.html',
		controller: 'consultaMedica'
	}).when('/consultaMedicaCreate/:id', {
		templateUrl: '/template/consultaMedicaCreate.html',
		controller: 'consultaMedicaCreate'
	}).otherwise({
		redirectTo: '/'
	})
	}).controller('consultaMedica', ['$scope','$http','$routeParams','DTOptionsBuilder','DTColumnBuilder','DTDefaultOptions', function ($scope,$http,$routeParams,DTOptionsBuilder, DTColumnBuilder,DTDefaultOptions){
		var id=$routeParams.id;
		$http({
			url:'/consulta/consultaMedica/'+id,
			data:{id:id},
			method:'get',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function(data){
			console.log(data);
			//$scope.consultaMedica=data;
			$scope.dataForm=data.id;

		$scope.consultaMedica = DTOptionsBuilder.fromSource(data.consultaMedica);
    	$scope.dtOptions_c = DTOptionsBuilder.newOptions().withPaginationType('full_numbers')
    //.withDOM('<div> holaaaaa </div>')
    .withLanguage({
    "sEmptyTable":     "No hay datos para cargar en la tabla",
    "sInfo":           "Mostrando _START_ de _END_ de _TOTAL_ entradas",
    "sInfoEmpty":      "Mostrando 0 de 0 de 0 entradas",
    "sInfoFiltered":   "(filtradas _MAX_ entradas totales)",
    "sInfoPostFix":    "",
    "sInfoThousands":  ",",
    "sLengthMenu":     "Mostrar _MENU_ entradas",
    "sLoadingRecords": "Cargando...",
    "sProcessing":     "Procesando...",
    "sSearch":         "Buscar:",
    "sZeroRecords":    "No se encontraron registros",
    "sColvis": "C<'clear'>lfrtip",
    "oPaginate": {
        "sFirst":    "Primera",
        "sLast":     "Última",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": activar para ordenar de forma ascendente",
        "sSortDescending": ": activar para ordenar de forma descendente"
    },
    "buttons": {
        exportOptions: {
            columns: ':visible'
        },
               print: 'Imprimir',
  }
  })

.withButtons([
           {
        extend: "copy",
        text:"Copiar",
        exportOptions: {
            columns: ':visible'
        },
        exportData: {decodeEntities:true}
    },
    {
        extend: "excel",
        filename:  "Data_Analysis",
        title:"Data Analysis Report",
        exportOptions: {
            columns: ':visible'
        },
        //CharSet: "utf8",
        exportData: { decodeEntities: true }
    },
    {
        extend: "csv",
        exportOptions: {
            columns: ':visible'
        },
        exportData: {decodeEntities:true}
    },


    {
        extend: "pdf",
        fileName:  "Data_Analysis",
        title:"Listado de Usuario",
        orientation: 'landscape',
         message: 'Usuario registrados en el sistema',
        customize: function (doc) {

                    doc.content.splice(1, 0, [{
                        margin: [0, 0, 0, 12],
                        width: 250,
                        message: 'Usuario registrados en el sistema',
                        alignment: 'center',
                        image: 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSZ9vXvMTrz5QAXziamO3piMhSm-o27jbpTEWFfsWBbIfnjAzuqpQ',
                        }
                        ])
                    },
        exportOptions: {
            columns: ':visible',
            customize: ':visible'
        },
        exportData: {decodeEntities:true}
    },

    {
        extend: 'print',
        //text: 'Print current page',
        autoPrint: false,
        exportOptions: {
            columns: ':visible'
        }
    },
    {
    text:"Columna Visible",
    extend:'colvis'
    }

]).withBootstrap();



		}).error(function(error){
			console.log(error);
		});
	}]).controller('consultaMedicaCreate', ['$scope','$http','$routeParams','$window', function ($scope,$http,$routeParams,$window){
		var id=$routeParams.id;
        //console.log(id);
		$scope.dataForm={
			id:id};
		//console.log($scope.dataForm);
		$http({
			url:'/consulta/consultaMedica/'+id,
			method:'put',
			data:$scope.form,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(data){
			$scope.listaEnfermedad=data.listaEnfermedad;
			$scope.listaMedico=data.listaMedico;
			$scope.listaMedicamento=data.listaMedicamento;
			$scope.paciente=data.paciente;
			$scope.nombreApellido=data.paciente.nombre+' '+data.paciente.apellido;
		}).error(function(error){
			console.log(error);
		});

		$scope.guardarConsultaMedica=function(){
			$http({
			url:'/consulta/consultaMedica',
			method:'post',
			data:{form:$scope.dataForm},
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(data){
			if(data.error==''){
			$scope.mensaje=data.mensaje;
			$scope.clase=data.clase;
			$scope.mostrarMedico='';
			$scope.mostrarEnfermedad='';
			$scope.mostrarObservacion='';
			$window.location.href = '/admin/usuario/create#/consultaMedica/'+id;
			}else{
				$scope.mostrarMedico=data.error.medico;
				$scope.mostrarEnfermedad=data.error.enfermedad;
				$scope.mostrarObservacion=data.error.observacion;
			}
		}).error(function(error){
			console.log(error);
		});
		}
		
	}]);