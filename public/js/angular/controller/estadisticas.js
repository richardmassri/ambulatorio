angular.module('app.moduloEstadisticas', []).
config(function ($interpolateProvider,$routeProvider,$locationProvider){
	$interpolateProvider.startSymbol('{{{');
    $interpolateProvider.endSymbol('}}}');
    $routeProvider
    .when('/estadisticas', {
    	controller: 'estadisticas',
    	 templateUrl: '/template/estadisticas.html'
    }).when('/estadisticasNumericas', {
    	templateUrl: '/template/estadisticasNumericas.html',
    	controller: 'EstadisticasNumericasCtrl'
    }).otherwise({
    	redirectTo: '/'
	})
}).controller('estadisticas', ['$scope','$http', function ($scope,$http){

$scope.myDataSource = {
    chart: {
        caption: "Consultas Medicas",
        subcaption: "Grafico",
        startingangle: "120",
        showlabels: "0",
        showlegend: "1",
        enablemultislicing: "0",
        slicingdistance: "15",
        showpercentvalues: "1",
        showpercentintooltip: "0",
        plottooltext: "Age group : $label Total visit : $datavalue",
        theme: "fint",
        exportEnabled: "1"
    },
    data: [
        {
            label: "Miranda",
            value: "1"
        },
        {
            label: "Trujillo",
            value: "1"
        },
        {
            label: "Falcon",
            value: "0"
        },
      {
            label: "DISTRITO CAPITAL",
            value: "0"
        },
        {
            label: "AMAZONAS",
            value: "0"
        },
                {
            label: "ANZOATEGUI",
            value: "0"
        },
                {
            label: "APURE",
            value: "0"
        },

        {
            label: "ARAGUA",
            value: "0"
        },

        {
            label: "BARINAS",
            value: "0"
        },
        {
            label: "BOLIVAR",
            value: "0"
        },
        {
            label: "CARABOBO",
            value: "0"
        },
        {
            label: "COJEDES",
            value: "0"
        },
        {
            label: "DELTA AMACURO",
            value: "0"
        },
        {
            label: "GUARICO",
            value: "0"
        },
        {
            label: "LARA",
            value: "0"
        },
        {
            label: "MERIDA",
            value: "0"
        },
        {
            label: "MONAGAS",
            value: "0"
        },
        {
            label: "NUEVA ESPARTA",
            value: "0"
        },
        {
            label: "PORTUGUESA",
            value: "0"
        },
        {
            label: "SUCRE",
            value: "0"
        },
        {
            label: "TACHIRA",
            value: "0"
        },
        {
            label: "YARACUY",
            value: "0"
        },
        {
            label: "ZULIA",
            value: "0"
        },
        {
            label: "VARGAS",
            value: "0"
        },
        {
            label: "DEPENDENCIAS FEDERALES",
            value: "0"
        }

    ]
}
console.log($scope.myDataSource);

	$scope.tipoReporte='pie3d';
	//$scope.tipo='barra';
	 $scope.$watch('tipo',function(nuevo,anterior){
            //console.log(nuevo);
            if(!nuevo) return;
            if(nuevo=="barra"){
            	$scope.tipoReporte='column2d';
					$scope.myDataSource.chart={
					caption: "Consulta Medica",
					subCaption: "Grafico",
					numberPrefix: "%",
					theme: "ocean"
					}
            }else{
				console.log(nuevo);
				$scope.tipoReporte='pie3d';
				$scope.myDataSource.chart={};
				$scope.myDataSource.chart={
				caption: "Consulta Medica",
				subcaption: "Grafico",
				startingangle: "120",
				showlabels: "0",
				showlegend: "1",
				enablemultislicing: "0",
				slicingdistance: "15",
				showpercentvalues: "1",
				showpercentintooltip: "0",
				plottooltext: "Age group : $label Total visit : $datavalue",
				theme: "fint",
				exportEnabled: "1"
				}
        }

        });
        	$http({
		url:'/consulta/estadisticas',
		method:'get',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(datos){

			//console.log($scope.myDataSource);
$scope["myDataSource"]["data"][0]["value"]=datos['consultaEstadisticas'][0]['miranda'];
$scope["myDataSource"]["data"][1]["value"]=datos['consultaEstadisticas'][0]['trujillo'];
$scope["myDataSource"]["data"][2]["value"]=datos['consultaEstadisticas'][0]['falcon'];
$scope["myDataSource"]["data"][3]["value"]=datos['consultaEstadisticas'][0]['distrito_capital'];
$scope["myDataSource"]["data"][4]["value"]=datos['consultaEstadisticas'][0]['amazonas'];
$scope["myDataSource"]["data"][5]["value"]=datos['consultaEstadisticas'][0]['anzoategui'];
$scope["myDataSource"]["data"][6]["value"]=datos['consultaEstadisticas'][0]['apure'];
$scope["myDataSource"]["data"][7]["value"]=datos['consultaEstadisticas'][0]['aragua'];
$scope["myDataSource"]["data"][8]["value"]=datos['consultaEstadisticas'][0]['barinas'];
$scope["myDataSource"]["data"][9]["value"]=datos['consultaEstadisticas'][0]['bolivar'];
$scope["myDataSource"]["data"][10]["value"]=datos['consultaEstadisticas'][0]['carabobo'];
$scope["myDataSource"]["data"][11]["value"]=datos['consultaEstadisticas'][0]['cojedes'];
$scope["myDataSource"]["data"][12]["value"]=datos['consultaEstadisticas'][0]['delta_amacuro'];
$scope["myDataSource"]["data"][13]["value"]=datos['consultaEstadisticas'][0]['guarico'];
$scope["myDataSource"]["data"][14]["value"]=datos['consultaEstadisticas'][0]['lara'];
$scope["myDataSource"]["data"][15]["value"]=datos['consultaEstadisticas'][0]['marida'];
$scope["myDataSource"]["data"][16]["value"]=datos['consultaEstadisticas'][0]['monagas'];
$scope["myDataSource"]["data"][17]["value"]=datos['consultaEstadisticas'][0]['nueva_esparta'];
$scope["myDataSource"]["data"][18]["value"]=datos['consultaEstadisticas'][0]['portuguesa'];
$scope["myDataSource"]["data"][19]["value"]=datos['consultaEstadisticas'][0]['sucre'];
$scope["myDataSource"]["data"][20]["value"]=datos['consultaEstadisticas'][0]['tachira'];
$scope["myDataSource"]["data"][21]["value"]=datos['consultaEstadisticas'][0]['yaracuy'];
$scope["myDataSource"]["data"][22]["value"]=datos['consultaEstadisticas'][0]['zulia'];
$scope["myDataSource"]["data"][23]["value"]=datos['consultaEstadisticas'][0]['vargas'];
$scope["myDataSource"]["data"][24]["value"]=datos['consultaEstadisticas'][0]['dependencias_federales'];



		}).error(function(error){
			
	});
}]).controller('EstadisticasNumericasCtrl', ['$scope','$http','$routeParams','$window','DTOptionsBuilder', function ($scope,$http,$routeParams,$window,DTOptionsBuilder){
	$http({
		url:'/consulta/estadisticas',
		method:'get',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(datos){
			//$scope.estadisticasNumericas=datos.consultaEstadisticas;
		$scope.mostrarMensajeExito='Exito';
		$scope.estadisticasNumericas = DTOptionsBuilder.fromSource(datos.consultaEstadisticas);
    	$scope.dtOptions_c = DTOptionsBuilder.newOptions().withPaginationType('full_numbers')
    //.withDOM('<div> holaaaaa </div>')
    .withLanguage({
    "sEmptyTable":     "No hay datos para cargar en la tabla",
    "sInfo":           "Mostrando _START_ de _END_ de _TOTAL_ entradas",
    "sInfoEmpty":      "Mostrando 0 de 0 de 0 entradas",
    "sInfoFiltered":   "(filtradas _MAX_ entradas totales)",
    "sInfoPostFix":    "",
    "sInfoThousands":  ",",
    "sLengthMenu":     "Mostrar _MENU_ entradas",
    "sLoadingRecords": "Cargando...",
    "sProcessing":     "Procesando...",
    "sSearch":         "Buscar:",
    "sZeroRecords":    "No se encontraron registros",
    "sColvis": "C<'clear'>lfrtip",
    "oPaginate": {
        "sFirst":    "Primera",
        "sLast":     "Última",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": activar para ordenar de forma ascendente",
        "sSortDescending": ": activar para ordenar de forma descendente"
    },
    "buttons": {
        exportOptions: {
            columns: ':visible'
        },
               print: 'Imprimir',
  }
  })

.withButtons([
           {
        extend: "copy",
        text:"Copiar",
        exportOptions: {
            columns: ':visible'
        },
        exportData: {decodeEntities:true}
    },
    {
        extend: "excel",
        filename:  "Data_Analysis",
        title:"Data Analysis Report",
        exportOptions: {
            columns: ':visible'
        },
        //CharSet: "utf8",
        exportData: { decodeEntities: true }
    },
    {
        extend: "csv",
        exportOptions: {
            columns: ':visible'
        },
        exportData: {decodeEntities:true}
    },


    {
        extend: "pdf",
        fileName:  "Data_Analysis",
        title:"Listado de Usuario",
        orientation: 'landscape',
         message: 'Usuario registrados en el sistema',
        customize: function (doc) {

                    doc.content.splice(1, 0, [{
                        margin: [0, 0, 0, 12],
                        width: 250,
                        message: 'Usuario registrados en el sistema',
                        alignment: 'center',
                        image: 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSZ9vXvMTrz5QAXziamO3piMhSm-o27jbpTEWFfsWBbIfnjAzuqpQ',
                        }
                        ])
                    },
        exportOptions: {
            columns: ':visible',
            customize: ':visible'
        },
        exportData: {decodeEntities:true}
    },

    {
        extend: 'print',
        //text: 'Print current page',
        autoPrint: false,
        exportOptions: {
            columns: ':visible'
        }
    }

]).withBootstrap();



			$scope.listado=datos.consultaEstadisticas;
			console.log($scope.listado);
		}).error(function(error){
			consolo.log(error);
	})
}])