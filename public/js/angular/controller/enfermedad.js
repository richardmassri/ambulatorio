(function(){
	angular.module('app.enfermedad',[]).config(function(hljsServiceProvider,$interpolateProvider,$routeProvider,$locationProvider) {
    hljsServiceProvider.setOptions({
        // replace tab with 4 spaces
        tabReplace: '    '
    });
    $interpolateProvider.startSymbol('{{{');
    $interpolateProvider.endSymbol('}}}');
    //$locationProvider.html5Mode(true);
    $routeProvider.when('/create_enfermedad', {
        templateUrl: '/template/create_enfermedad.html',
       
    }).when('/listado_enfermedad', {
        controller: 'listadoEnfermedad',
        templateUrl: '/template/listado_enfermedad.html'
    }).when('/view_enfermedad/:id', {
        templateUrl: '/template/view_enfermedad.html',
        controller: 'editarEnfermedad'   
    }).when('/edita_enfermedad/:id', {
        templateUrl: '/template/editar_enfermedad.html',
        controller: 'editarEnfermedad' 
    }).otherwise({
        redirectTo: '/'
    })

    }).controller('listadoEnfermedad',function($scope, DTOptionsBuilder, DTColumnBuilder,DTDefaultOptions,$http,$modal){

        $scope.showConfirm = function(id,estatus) {
            console.log(estatus);
            $modal.showModal = true;
            // Appending dialog to document.body to cover sidenav in docs app
            var modalInstance=$modal.open(
                {
                templateUrl:(estatus=='A')?'/template/delete_enfermedad.html':'/template/reactivar_enfermedad.html',
                controller: 'deleteEnfermedad',
                size: 'lg',
                resolve: {
                    id: function () {return id;},
                    estatus: function () {return estatus;}
            }

            });
        };

       //$scope.nombreM="Rida";

        $http({
            'url':'/enfermedad/lista/enfermedad',
            'data':{},
            'dataType': "json",
            'method':'POST',
             headers: {'Content-Type': 'application/x-www-form-urlencoded'}


        }).success(function(data){
            $scope.dtInstance={};
            //$scope.persons = $resource('data.json').query();
    $scope.listado = DTOptionsBuilder.fromSource(data.listado);
    $scope.dtOptions = DTOptionsBuilder.newOptions('aaSorting', [3, 'desc']).withPaginationType('full_numbers')
    //.withDOM('<div> holaaaaa </div>')
    .withLanguage({
    "sEmptyTable":     "No hay datos para cargar en la tabla",
    "sInfo":           "Mostrando _START_ de _END_ de _TOTAL_ entradas",
    "sInfoEmpty":      "Mostrando 0 de 0 de 0 entradas",
    "sInfoFiltered":   "(filtradas _MAX_ entradas totales)",
    "sInfoPostFix":    "",
    "sInfoThousands":  ",",
    "sLengthMenu":     "Mostrar _MENU_ entradas",
    "sLoadingRecords": "Cargando...",
    "sProcessing":     "Procesando...",
    "sSearch":         "Buscar:",
    "sZeroRecords":    "No se encontraron registros",
    "sColvis": "C<'clear'>lfrtip",
    "oPaginate": {
        "sFirst":    "Primera",
        "sLast":     "Última",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": activar para ordenar de forma ascendente",
        "sSortDescending": ": activar para ordenar de forma descendente"
    },
    "buttons": {
        exportOptions: {
            columns: ':visible'
        },
               print: 'Imprimir',
  }
  })

.withButtons([
           {
        extend: "copy",
        text:"Copiar",
        exportOptions: {
            columns: ':visible'
        },
        exportData: {decodeEntities:true}
    },
    {
        extend: "excel",
        filename:  "Data_Analysis",
        title:"Data Analysis Report",
        exportOptions: {
            columns: ':visible'
        },
        //CharSet: "utf8",
        exportData: { decodeEntities: true }
    },
    {
        extend: "csv",
        exportOptions: {
            columns: ':visible'
        },
        exportData: {decodeEntities:true}
    },


    {
        extend: "pdf",
        fileName:  "Data_Analysis",
        title:"Listado de Enfermedades",
        orientation: 'landscape',
         message: 'Enfermedades registradas en el sistema',
        customize: function (doc) {

                    doc.content.splice(1, 0, [{
                        margin: [0, 0, 0, 12],
                        width: 250,
                        message: 'Enfermedades registradas en el sistema',
                        alignment: 'center',
                        image: 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSZ9vXvMTrz5QAXziamO3piMhSm-o27jbpTEWFfsWBbIfnjAzuqpQ',
                        }
                        ])
                    },
        exportOptions: {
            columns: ':visible',
            customize: ':visible'
        },
        exportData: {decodeEntities:true}
    },

    {
        extend: 'print',
        //text: 'Print current page',
        autoPrint: false,
        exportOptions: {
            columns: ':visible'
        }
    },
    {
    text:"Columna Visible",
    extend:'colvis'
    }

]).withBootstrap();

  console.log($scope.listado);


        }).error(function(error){
            console.log(error);

        });

       
}).controller('GuardarEnfermedadController',function($scope,$http,$window){
        

        $scope.$watch('dataForm.nombre',function(nuevo,anterio){
            if(!nuevo) return;
            if(nuevo.length<1){
            $scope.mostrarNombre="El nombre debe contener al menos 1 letras";
            }else{
                $scope.mostrarNombre=false;
            }
        });

        $scope.dataForm = {};

           $scope.guardarEnfermedad = function () {
            
                angular.element('#btn-consultar')
                    .html('<i class="fa fa-spinner fa-spin fa-lg fa-fw"></i> Consultado...')
                    .prop('disabled', true);
                $http({
                    method: 'POST',
                    url: '/admin/enfermedad',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data:{
                        'form': $scope.dataForm
                    }
                }).success(function(data){
                        console.log(data['error']);
                        if(data.statusCode.mensaje!=false){
                            $scope.mostrarMensajeExito=data.statusCode.mensaje;
                            console.log($scope.mostrarMensajeExito);
                        }else{
                            $scope.mostrarNombre=data.error.name;

                        }
                            //$window.location.href = '/admin/usuario/create';
                        }).error(function(){
                 angular.element('#btn-consultar')
                            .html('<i class="fa fa-search"></i> Consultar')
                            .prop('disabled', false);

                        //Recargamos el recaptcha
                        //$scope.reaload_widget($scope.widgetId);
                });
                }
    }).controller('editarEnfermedad',function($scope,$http,$routeParams){

        var id=$routeParams.id;
       
        $http({
            'url':'/admin/enfermedad/'+id,
            'data':{id:id},
            'method':'get',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(datos){
            console.log(datos);
            $scope.dataForm=datos.dataForm;
        }).error(function(error){
            console.log(error);
        })
        
        $scope.$watch('dataForm.nombre',function(nuevo,anterio){
            console.log(nuevo);
            if(!nuevo) return;
            if(nuevo.length<1){
                $scope.mostrarNombre="El nombre debe contener al menos 1 letras";
            }else{
                $scope.mostrarNombre=false;
            }
        });

        $scope.$watch('dataForm.estatus',function(nuevo,anterio){
            if(!nuevo) return;
            if(nuevo.length<1){
                $scope.mostrarEstatus="El estatus debe contener al menos 1 letras";
            }else{
                $scope.mostrarEstatus=false;
            }
        });

       $scope.editarEnfermedad = function () {
                console.log($scope.dataForm);
                angular.element('#btn-consultar')
                    .html('<i class="fa fa-spinner fa-spin fa-lg fa-fw"></i> Consultado...')
                    .prop('disabled', true);
                $http({
                    method: 'PUT',
                    url: '/admin/enfermedad/'+$scope.dataForm.id,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data:{
                        'form': $scope.dataForm
                    }
                }).success(function(data){
                        console.log(data['error']);
                        if(data.statusCode.mensaje!=false){
                            $scope.mostrarMensajeExito=data.statusCode.mensaje;
                            console.log($scope.mostrarMensajeExito);
                        }else{
                            $scope.mostrarNombre=data.error.name;
                            $scope.mostrarEstatus=data.error.estatus;

                        }

                        }).error(function(){
                 angular.element('#btn-consultar')
                            .html('<i class="fa fa-search"></i> Consultar')
                            .prop('disabled', false);

                });
                }
    }).controller('deleteEnfermedad', function ($scope,id,estatus,$modalInstance,$http,$window,$timeout){
        var modalInstance=$scope.$modalInstance;
        $scope.cancel = function () {
         $modalInstance.close();
        };
          $scope.ok = function(){
            $http({
                    'url':'/admin/enfermedad/'+id+"-"+estatus,
                    'data':{id:id,estatus:estatus},
                    'method':'delete',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(datos){
                    console.log(datos);
                    $scope.mostrarMensajeExito=datos.statusCode.mensaje;
                    $timeout(function(){
                        $modalInstance.close();
                        $window.location.href='#listado_enfermedad';
                    },800);
                    //$scope.listado=datos['usuario'];
                    //alert("aquiii");
                    //$window.location.href='/admin/usuario/create#/buscar_paciente';
                    
                    
                    //$window.location.reload();
                }).error(function(error){
                    console.log(error);
                })
        };
    });

})();