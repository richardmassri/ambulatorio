<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', function () {
        return view('welcome');
    });
    /*Route::get('persona	/{nombre}', function($nombre){
   echo "El nombre que has introducido es:". $nombre;
    });*/
    /*Route::group(['prefix' => 'persona'],function(){
	Route::get	('view/{nombre?}',function($nombre="vacio"){
	echo "El nombre que usted ha introducido es:". $nombre;
});
});*/


   /* Route::group(['prefix' => 'persona'],function(){
	Route::get('view/{id}',[
       'uses'=>'personaController@index',
       'as' =>'personaView'
		]);
});*/

  Route::group(['prefix' => 'admin'],function(){
    Route::resource('persona', 'PersonaController');
});

  Route::group(['prefix' => 'admin'],function(){
    Route::resource('usuario', 'usuarioController');
});


  Route::group(['prefix' => 'admin'],function(){
    Route::resource('enfermedad', 'enfermedadController');
});

  Route::group(['prefix' => 'admin'],function(){
    Route::resource('utencilio', 'utencilioController');
});

  Route::group(['prefix' => 'admin'],function(){
    Route::resource('grupo', 'grupoController');
});

Route::group(['prefix' => 'admin'],function(){
  Route::resource('estado', 'estadoController');
});

Route::group(['prefix' => 'admin'],function(){
   Route::resource('medicamento', 'medicamentoController');
});


Route::group(['prefix' => 'consulta'],function(){
    Route::resource('paciente', 'pacienteController');
});

Route::group(['prefix' => 'consulta'],function(){
    Route::resource('consultaMedica', 'consultaMedicaController');
});

Route::group(['prefix' => 'medico'],function(){
    Route::resource('medico', 'medicoController');
});

Route::group(['prefix' => 'consulta'],function(){
    Route::resource('estadisticas', 'estadisticasController');
});

  //login de usuario
  Route::group(['prefix' => 'login'],function(){
  Route::get('main',[
       'uses'=>'loginController@index',
       //'uses'=>'loginController@consulta',
       'as' =>'loginMain'
    ]);
    Route::post('principal',[
       'uses'=>'loginController@postConsulta',
       //'uses'=>'loginController@consulta',
       'as' =>'consultaLogin'
    ]);
        Route::post('lista/usuario',[
       'uses'=>'usuarioController@listaUser',
       //'uses'=>'loginController@consulta',
       'as' =>'usuarioController'
    ]);
        Route::post('lista/campos/usuario',[
       'uses'=>'usuarioController@listaCampoUser',
       //'uses'=>'loginController@consulta',
       'as' =>'usuarioController'
    ]);
  });


  Route::group(['prefix' => 'enfermedad'],function(){

        Route::post('lista/enfermedad',[
       'uses'=>'enfermedadController@listaEnfermedad',
       //'uses'=>'loginController@consulta',
       'as' =>'enfermedadController'
    ]);
       
  });

  Route::group(['prefix' => 'admin'],function(){

        Route::post('lista/utencilio',[
       'uses'=>'utencilioController@listautencilio',
       'as' =>'utencilioController'
        ]);
        
  });

  Route::group(['prefix' => 'admin'],function(){

        Route::post('lista/grupo',[
       'uses'=>'grupoController@listagrupo',
       'as' =>'grupoController'
        ]);
        
  });

  Route::group(['prefix' => 'admin'],function(){

        Route::post('lista/estado',[
       'uses'=>'estadoController@listaestado',
       'as' =>'estadoController'
        ]);
        
  });

  Route::group(['prefix' => 'admin'],function(){

        Route::post('lista/medicamento',[
       'uses'=>'medicamentoController@listamedicamento',
       'as' =>'medicamentoController'
        ]);
        
  });

});