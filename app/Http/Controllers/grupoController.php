<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Grupo;

class grupoController extends Controller
{
    public function  create(Request $request){
		//dd($request['json']); die();
		//return view('userGroup.usuario');
		return view('layaut.main');
	}

	public function listaGrupo(){
		$grupo=Grupo::all();
		echo json_encode(['listado'=>$grupo],true);die();

	}

	public function  index(Request $request){
		return view('layaut.main');
	}

	public function show($id){

		$dataForm=Grupo::find($id);
		echo json_encode(['dataForm'=>$dataForm],true);
		die();

	}

	public function store(Request $request){

      	$request = json_decode(file_get_contents('php://input'), true);
		$nombre=isset($request['form']['nombre'])?$request['form']['nombre']:'';
		
		if($nombre==''){
			$error['name']="El nombre no debe ser vacio";
		}elseif(strlen($nombre)<=1){
			$error['name']="El nombre debe contener al menos 1 letra";
		}
		

      	if(isset($error)){
      		$error=$error;
      		$statusCode['mensaje']=false;
      		//$statusCode['exito']="error";
      	}else{
      		
      		$consulta_grupo=Grupo::where('nombre',$nombre)->get();

      		if(count($consulta_grupo)<=0){
	      		$grupo= new Grupo($request['form']);
	      		$grupo->estatus="A";
	      		$grupo->created_at=date('Y-m-d');
	      		$grupo->updated_at=date('Y-m-d');
	      		$grupo->save();
	      		$statusCode['mensaje']="El registro se ha guardado de forma exitosa";
	      		$error=false;
      		}else{
      			$error['estatus']=true;
	      		$statusCode['mensaje']="El grupo ya existe";
	      		$clase='warning';
      		}
      		
      	}
      	return response()->json(['error'=>$error,'statusCode'=>$statusCode]);
      	
	}

	public function update(Request $request,$id){
		$grupo=grupo::find($id);
		//var_dump($usuario);die();
		$request = json_decode(file_get_contents('php://input'), true);
		$nombre=isset($request['form']['nombre'])?$request['form']['nombre']:'';
		$estatus=isset($request['form']['estatus'])?$request['form']['estatus']:'';
		if($nombre==''){
			$error['name']="El nombre no debe ser vacio";
		}
		if($estatus==''){
			$error['estatus']="El estatus no debe ser vacio";
		}
      	if(isset($error)){
      		$error=$error;
      		$statusCode['mensaje']=false;
      		//$statusCode['exito']="error";
      	}else{
      		//dd($request['form']); die();
      		$grupo=Grupo::find($id);
      		$grupo->fill($request['form']);
      		//$grupo->created_at=date('Y-m-d'); modifica la fecha de creacion?
      		$grupo->updated_at=date('Y-m-d');
      		$grupo->save();
      		$statusCode['mensaje']="El registro se ha Modificado de forma exitosa";
      		$error=false;
      	}
      	return response()->json(['error'=>$error,'statusCode'=>$statusCode]);
	}

	public function destroy($id){

		$separar=explode("-", $id);
		$id=$separar[0];
		$estatus=$separar[1];
 		$grupo=Grupo::find($id);

		if($estatus=='A'){
 		$grupo->estatus='E';

 		$statusCode['mensaje']="El grupo se a eliminado de forma exitosa";
		}else{
		$grupo->estatus='A';
		$statusCode['mensaje']="El grupo se a activado de forma exitosa";

		}
		$grupo->save();
 		return response()->json(['statusCode'=>$statusCode]);

	}
}
