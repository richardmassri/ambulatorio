<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Atencion;
use App\Enfermedad;
use App\Medicamento;
use App\Medico;
use App\Paciente;
use DB;

class consultaMedicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$mensaje='';
    	$clase='';
    	$consultaMedica=new Atencion();
    	$request = json_decode(file_get_contents('php://input'), true);
    	$request['form']['id']=isset($request['form']['id'])?$consultaMedica->paciente_id=$request['form']['id']:'';
    	
    	$request['form']['enfermedad']=isset($request['form']['enfermedad'])?$consultaMedica->enfermedad_id=$request['form']['enfermedad']:'';
    	$request['form']['medico']=isset($request['form']['medico'])?$consultaMedica->medico_id=$request['form']['medico']:'';
    	$request['form']['observacion']=isset($request['form']['observacion'])?$consultaMedica->observacion=$request['form']['observacion']:'';
    	$consultaMedica->fecha=date('Y-m-d');
    	if($request['form']['enfermedad']=='' || empty($request['form']['enfermedad'])){
    		$error['enfermedad']="Este campo no debe ser vacio";
    	}
    	if($request['form']['medico']=='' || empty($request['form']['medico'])){
    		$error['medico']="Este campo no debe ser vacio";
    	}
    	if($request['form']['observacion']=='' || empty($request['form']['observacion'])){
    		$error['observacion']="Este campo no debe ser vacio";
    	}
    	if(isset($error)){
    		$error=$error;
    	}else{
    		$error='';
    	if($consultaMedica->save()){
    		$clase='success';
    		$mensaje="Se ha creao la consulta de forma exitosa";
    	}else{
    		$clase='error';
    		$mensaje="Ocurrio un error inesperado, por favor intentar mas tarde";
    	}
    }
    	return response()->json(['mensaje'=>$mensaje,'clase'=>$clase,'error'=>$error]);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$consultaMedica=DB::table('atenciones')->select('atenciones.observacion','atenciones.id','atenciones.fecha','enfermedades.nombre as enfermedad','pacientes.nombre as paciente','medicos.nombre as medico')->join('enfermedades','enfermedades.id','=','atenciones.enfermedad_id')->join('pacientes','pacientes.id','=','atenciones.paciente_id')->join('medicos','atenciones.medico_id','=','medicos.id')->where('atenciones.paciente_id','=',$id)->get();
    	return response()->json(['consultaMedica'=>$consultaMedica,'id'=>$id]);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$listaEnfermedad=Enfermedad::all();
    	$listaMedicamento=Medicamento::all();
    	$listaMedico=Medico::all();
    	$paciente=Paciente::find($id);
    	return response()->json(['listaEnfermedad'=>$listaEnfermedad,'listaMedico'=>$listaMedico,'listaMedicamento'=>$listaMedicamento,'paciente'=>$paciente]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
