<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Medicamento;
use Session;

class medicamentoController extends Controller
{
    public function  create(Request $request){
		//dd($request['json']); die();
		//return view('userGroup.usuario');
		return view('layaut.main');
	}

	public function listamedicamento(){
		$medicamento=Medicamento::all();
		echo json_encode(['listado'=>$medicamento],true);die();

	}

	public function  index(Request $request){
		return view('layaut.main');
	}

	public function show($id){

		$dataForm=Medicamento::find($id);
		echo json_encode(['dataForm'=>$dataForm],true);
		die();

	}

	public function store(Request $request){
		
      	$request = json_decode(file_get_contents('php://input'), true);
		$nombre=isset($request['form']['nombre'])?$request['form']['nombre']:'';
		
		if($nombre==''){
			$error['name']="El nombre no debe ser vacio";
		}elseif(strlen($nombre)<=1){
			$error['name']="El nombre debe contener al menos 1 letra";
		}
		

      	if(isset($error)){
      		$error=$error;
      		$statusCode['mensaje']=false;
      		//$statusCode['exito']="error";
      	}else{
      		
      		$consulta_medicamento=Medicamento::where('nombre',$nombre)->get();

      		if(count($consulta_medicamento)<=0){
	      		$medicamento= new Medicamento($request['form']);
	      		$medicamento->estatus="A";
	      		$medicamento->created_at=date('Y-m-d');
	      		$medicamento->updated_at=date('Y-m-d');
	      		$medicamento->usuario_ini_id=Session::get('usuario_id');
	      		$medicamento->usuario_act_id=Session::get('usuario_id');
	      		$medicamento->usuario_eli_id=Session::get('usuario_id');
	      		$medicamento->save();
	      		$statusCode['mensaje']="El registro se ha guardado de forma exitosa";
	      		$error=false;
      		}else{
      			$error['estatus']=true;
	      		$statusCode['mensaje']="El medicamento ya existe";
	      		$clase='warning';
      		}
      		
      	}
      	return response()->json(['error'=>$error,'statusCode'=>$statusCode]);
      	
	}

	public function update(Request $request,$id){
		$medicamento=Medicamento::find($id);
		//var_dump($usuario);die();
		$request = json_decode(file_get_contents('php://input'), true);
		$nombre=isset($request['form']['nombre'])?$request['form']['nombre']:'';
		$estatus=isset($request['form']['estatus'])?$request['form']['estatus']:'';
		if($nombre==''){
			$error['name']="El nombre no debe ser vacio";
		}
		if($estatus==''){
			$error['estatus']="El estatus no debe ser vacio";
		}
      	if(isset($error)){
      		$error=$error;
      		$statusCode['mensaje']=false;
      		//$statusCode['exito']="error";
      	}else{
      		//dd($request['form']); die();
      		$medicamento=Medicamento::find($id);
      		$medicamento->fill($request['form']);
      		$medicamento->updated_at=date('Y-m-d');
      		$medicamento->save();
      		$statusCode['mensaje']="El registro se ha Modificado de forma exitosa";
      		$error=false;
      	}
      	return response()->json(['error'=>$error,'statusCode'=>$statusCode]);
	}

	public function destroy($id){

		$separar=explode("-", $id);
		$id=$separar[0];
		$estatus=$separar[1];
 		$medicamento=Medicamento::find($id);

		if($estatus=='A'){
 		$medicamento->estatus='E';

 		$statusCode['mensaje']="El medicamento se a eliminado de forma exitosa";
		}else{
		$medicamento->estatus='A';
		$statusCode['mensaje']="El medicamento se a activado de forma exitosa";

		}
		$medicamento->save();
 		return response()->json(['statusCode'=>$statusCode]);

	}
}
