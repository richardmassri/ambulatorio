<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Paciente;
use App\Estado;
use Session;

class pacienteController extends Controller
{

	public function show($id){
		$dataForm=Paciente::find($id);
		$estado=$dataForm->estado_paciente;
		//$estado = $dataForm->estado_paciente;
		//var_dump($estado); die();
		return response()->json(['dataForm'=>$dataForm,'estado'=>$estado]);
	}

public function store(Request $request){
	$clase='';
	$status='';
	$request = json_decode(file_get_contents('php://input'), true);
	if(isset($request['dataForm'])){
		//dd($request['dataForm']['cedula']);
		$consultarPaciente=Paciente::where('cedula',$request['dataForm']['cedula'])->get();
		if(count($consultarPaciente)>0){
			$clase='warning';
			$status['mensaje']="El paciente ya existe";
		return response()->json(['status'=>$status,'clase'=>$clase]);
	}else{
		$apellido=isset($request['dataForm']['apellido'])?$request['dataForm']['apellido']:'';
		$nombre=isset($request['dataForm']['nombre'])?$request['dataForm']['nombre']:'';
		$telefono_celular=isset($request['dataForm']['telefono_celular'])?$request['dataForm']['telefono_celular']:'';
		$estado_id=isset($request['dataForm']['estado_id'])?$request['dataForm']['estado_id']:'';
		$correo=isset($request['dataForm']['correo'])?$request['dataForm']['correo']:'';
		$estatus=isset($request['dataForm']['estatus'])?$request['dataForm']['estatus']:'';
		$direccion=isset($request['dataForm']['direccion'])?$request['dataForm']['direccion']:'';
		if($apellido==''){
			$error['apellido']="El apellido no debe ser vacio";
		}
		if($nombre==''){
			$error['nombre']="El nombre no debe ser vacio";
		}
		if($correo==''){
			$error['correo']="El correo no debe ser vacio";
		}
		if($telefono_celular==''){
			$error['telefono_celular']="El telefono célular no debe ser vacio";
		}
		if($estado_id==''){
			$error['estado']="El estado no debe ser vacio";	
		}
		if($estatus==''){
			$error['estatus']="El estatus no debe ser vacio";
		}
		if($direccion==''){
			$error['direccion']="La dirección no debe ser vacio";
		}
		if(isset($error)){
			$error=$error;
		}else{
			$error='';
			$clase='success';
			$paciente = new Paciente($request['dataForm']);
			$paciente->telefono=$request['dataForm']['telefono_celular'];
			$paciente->usuario_ini_id=Session::get('usuario_id');
			$paciente->usuario_act_id=Session::get('usuario_id');
			$paciente->usuario_eli_id=Session::get('usuario_id');
			//$paciente->estado_id=1;
			$paciente->municipio_id=1;
			$paciente->parroquia_id=1;
			$paciente->estatus='A';
			$paciente->save();
			$status['mensaje']="El paciente se ha creado de forma exitosa";
		}
		return response()->json(['status'=>$status,'clase'=>$clase,'error'=>$error]);
	}
	}

//dd($request['cedula']);
//dd($paciente); die();
//if(!is_numeric($request['cedula'])
if(isset($request['cedula'])){
	//dd("aquii");
$cedula=(int)$request['cedula'];
//dd($cedula);
if(!is_numeric($cedula)){
	$error['mensaje']="La cédula debe ser solo numerica";
	return response()->json(['statusCode'=>'errorCedulaMenor','mensaje'=>$error]);
}elseif(strlen($cedula)<8){
	$error['mensaje']="La cédula debe ser de 8 numeros";
	return response()->json(['statusCode'=>'errorCedulaNumeric','mensaje'=>$error]);
}else{
$paciente=Paciente::where('cedula',$request['cedula'])->get();
	if(count($paciente)>0){
	return response()->json(['statusCode'=>'mostrarConsulta','listado'=>$paciente]);
	//return response()->Sjson(['error'=>$error,'statusCode'=>$statusCode]);
}else{
	$estado=Estado::all();
	return response()->json(['statusCode'=>'mostrarPaciente','estado'=>$estado]);
}
}
}
}
}
