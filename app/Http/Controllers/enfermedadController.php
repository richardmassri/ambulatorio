<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Enfermedad;
use Session;

class enfermedadController extends Controller
{
    public function  create(Request $request){
		//dd($request['json']); die();
		//return view('userGroup.usuario');
		return view('layaut.main');
	}

	public function listaEnfermedad(){
		//dd("aquiii");die();
		$enfermedad=Enfermedad::all();
		//$enfermedad=DB::table('enfermedades')->orderBy('nombre','asc')->get();
		echo json_encode(['listado'=>$enfermedad],true);die();

	}

	public function  index(Request $request){
		return view('layaut.main');
	}

	public function store(Request $request){
		//$error[]='';
		$request = json_decode(file_get_contents('php://input'), true);
		$nombre=isset($request['form']['nombre'])?$request['form']['nombre']:'';
		
		if($nombre==''){
			$error['name']="El nombre no debe ser vacio";
		}elseif(strlen($nombre)<=1){
			$error['name']="El nombre debe contener al menos 1 letra";
		}
		

      	if(isset($error)){
      		$error=$error;
      		$statusCode['mensaje']=false;
      		//$statusCode['exito']="error";
      	}else{
      		$consulta_enfermedad=Enfermedad::where('nombre',$nombre)->get();
      		if(count($consulta_enfermedad)<=0){
	      		$enfermedad= new Enfermedad($request['form']);
	      		$enfermedad->estatus="A";
	      		//var_dump(Session::get('usuario_id')); die();
	      		$enfermedad->created_at=date('Y-m-d');
	      		$enfermedad->updated_at=date('Y-m-d');
	      		$enfermedad->usuario_ini_id=Session::get('usuario_id');
	      		$enfermedad->usuario_act_id=Session::get('usuario_id');
	      		$enfermedad->usuario_eli_id=Session::get('usuario_id');
	      		$enfermedad->save();
	      		$statusCode['mensaje']="El registro se ha guardado de forma exitosa";
	      		$error=false;
      		}else{
      			$error['estatus']=true;
	      		$statusCode['mensaje']="La enfermedad ya existe";
	      		$clase='warning';
      		}
      		
      	}
      	return response()->json(['error'=>$error,'statusCode'=>$statusCode]);
      	
	}

	public function show($id){

		$dataForm=Enfermedad::find($id);
		echo json_encode(['dataForm'=>$dataForm],true);
		die();

	}

	public function update(Request $request,$id){
		$enfermedad=Enfermedad::find($id);
		$request = json_decode(file_get_contents('php://input'), true);
		$nombre=isset($request['form']['nombre'])?$request['form']['nombre']:'';
		$estatus=isset($request['form']['estatus'])?$request['form']['estatus']:'';

		if($nombre==''){
			$error['name']="El nombre no debe ser vacio";
		}

		if($estatus==''){
			$error['estatus']="El estatus no debe ser vacio";
		}

		if(isset($error)){
      		$error=$error;
      		$statusCode['mensaje']=false;
      		//$statusCode['exito']="error";
      	}else{
      		//dd($request['form']); die();
      		$enfermedad=Enfermedad::find($id);
      		$enfermedad->fill($request['form']);
      		$enfermedad->updated_at=date('Y-m-d');
      		$enfermedad->save();
      		$statusCode['mensaje']="El registro se ha Modificado de forma exitosa";
      		$error=false;
      	}
      	return response()->json(['error'=>$error,'statusCode'=>$statusCode]);
	}

	public function destroy($id){

		$separar=explode("-", $id);
		$id=$separar[0];
		$estatus=$separar[1];
 		$enfermedad=Enfermedad::find($id);

		if($estatus=='A'){
 		$enfermedad->estatus='E';

 		$statusCode['mensaje']="La enfermedad se a eliminado de forma exitosa";
		}else{
		$enfermedad->estatus='A';
		$statusCode['mensaje']="La enfermedad se a activado de forma exitosa";

		}
		$enfermedad->save();
 		return response()->json(['statusCode'=>$statusCode]);
	}
}
