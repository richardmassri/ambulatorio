<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Estado;

class estadoController extends Controller
{
    public function  create(Request $request){
		//dd($request['json']); die();
		//return view('userGroup.usuario');
		return view('layaut.main');
	}

	public function listaEstado(){
		$estado=Estado::all();
		echo json_encode(['listado'=>$estado],true);die();

	}

	public function  index(Request $request){
		return view('layaut.main');
	}

	public function show($id){

		$dataForm=Estado::find($id);
		echo json_encode(['dataForm'=>$dataForm],true);
		die();

	}

	public function store(Request $request){
		
      	$request = json_decode(file_get_contents('php://input'), true);
		$nombre=isset($request['form']['nombre'])?$request['form']['nombre']:'';
		
		if($nombre==''){
			$error['name']="El nombre no debe ser vacio";
		}elseif(strlen($nombre)<=1){
			$error['name']="El nombre debe contener al menos 1 letra";
		}
		

      	if(isset($error)){
      		$error=$error;
      		$statusCode['mensaje']=false;
      		//$statusCode['exito']="error";
      	}else{
      		
      		$consulta_estado=Estado::where('nombre',$nombre)->get();

      		if(count($consulta_estado)<=0){
	      		$estado= new Estado($request['form']);
	      		$estado->estatus="A";
	      		$estado->created_at=date('Y-m-d');
	      		$estado->updated_at=date('Y-m-d');
	      		$estado->save();
	      		$statusCode['mensaje']="El registro se ha guardado de forma exitosa";
	      		$error=false;
      		}else{
      			$error['estatus']=true;
	      		$statusCode['mensaje']="El estado ya existe";
	      		$clase='warning';
      		}
      		
      	}
      	return response()->json(['error'=>$error,'statusCode'=>$statusCode]);
      	
	}

	public function update(Request $request,$id){
		$estado=Estado::find($id);
		//var_dump($usuario);die();
		$request = json_decode(file_get_contents('php://input'), true);
		$nombre=isset($request['form']['nombre'])?$request['form']['nombre']:'';
		$estatus=isset($request['form']['estatus'])?$request['form']['estatus']:'';
		if($nombre==''){
			$error['name']="El nombre no debe ser vacio";
		}
		if($estatus==''){
			$error['estatus']="El estatus no debe ser vacio";
		}
      	if(isset($error)){
      		$error=$error;
      		$statusCode['mensaje']=false;
      		//$statusCode['exito']="error";
      	}else{
      		//dd($request['form']); die();
      		$estado=Estado::find($id);
      		$estado->fill($request['form']);
      		//$estado->created_at=date('Y-m-d'); modifica la fecha de creacion?
      		$estado->updated_at=date('Y-m-d');
      		$estado->save();
      		$statusCode['mensaje']="El registro se ha Modificado de forma exitosa";
      		$error=false;
      	}
      	return response()->json(['error'=>$error,'statusCode'=>$statusCode]);
	}

	public function destroy($id){

		$separar=explode("-", $id);
		$id=$separar[0];
		$estatus=$separar[1];
 		$estado=Estado::find($id);

		if($estatus=='A'){
 		$estado->estatus='E';

 		$statusCode['mensaje']="El estado se a eliminado de forma exitosa";
		}else{
		$estado->estatus='A';
		$statusCode['mensaje']="El estado se a activado de forma exitosa";

		}
		$estado->save();
 		return response()->json(['statusCode'=>$statusCode]);

	}
}
