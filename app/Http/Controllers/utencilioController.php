<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Utencilio;
use Session;

class utencilioController extends Controller
{
    public function  create(Request $request){
		//dd($request['json']); die();
		//return view('userGroup.usuario');
		return view('layaut.main');
	}

	public function listaUtencilio(){
		$utencilio=Utencilio::all();
		echo json_encode(['listado'=>$utencilio],true);die();

	}

	public function  index(Request $request){
		return view('layaut.main');
	}

	public function show($id){

		$dataForm=Utencilio::find($id);
		echo json_encode(['dataForm'=>$dataForm],true);
		die();

	}

	public function store(Request $request){

      	$request = json_decode(file_get_contents('php://input'), true);
		$nombre=isset($request['form']['nombre'])?$request['form']['nombre']:'';
		
		if($nombre==''){
			$error['name']="El nombre no debe ser vacio";
		}elseif(strlen($nombre)<=1){
			$error['name']="El nombre debe contener al menos 1 letra";
		}
		

      	if(isset($error)){
      		$error=$error;
      		$statusCode['mensaje']=false;
      		//$statusCode['exito']="error";
      	}else{
      		
      		$consulta_utencilio=Utencilio::where('nombre',$nombre)->get();
      		if(count($consulta_utencilio)<=0){
	      		$utencilio= new Utencilio($request['form']);
	      		$utencilio->estatus="A";
	      		$utencilio->created_at=date('Y-m-d');
	      		$utencilio->updated_at=date('Y-m-d');
	      		$utencilio->usuario_ini_id=Session::get('usuario_id');
	      		$utencilio->usuario_act_id=Session::get('usuario_id');
	      		$utencilio->usuario_eli_id=Session::get('usuario_id');
	      		$utencilio->save();
	      		$statusCode['mensaje']="El registro se ha guardado de forma exitosa";
	      		$error=false;
      		}else{
      			$error['estatus']=true;
	      		$statusCode['mensaje']="El utencilio ya existe";
	      		$clase='warning';
      		}
      	}
      	return response()->json(['error'=>$error,'statusCode'=>$statusCode]);
	}

	public function update(Request $request,$id){
		$utencilio=Utencilio::find($id);
		//var_dump($usuario);die();
		$request = json_decode(file_get_contents('php://input'), true);
		$nombre=isset($request['form']['nombre'])?$request['form']['nombre']:'';
		$estatus=isset($request['form']['estatus'])?$request['form']['estatus']:'';
		if($nombre==''){
			$error['name']="El nombre no debe ser vacio";
		}
		if($estatus==''){
			$error['estatus']="El estatus no debe ser vacio";
		}
      	if(isset($error)){
      		$error=$error;
      		$statusCode['mensaje']=false;
      		//$statusCode['exito']="error";
      	}else{
      		//dd($request['form']); die();
      		$utencilio=Utencilio::find($id);
      		$utencilio->fill($request['form']);
      		//$utencilio->created_at=date('Y-m-d'); modifica la fecha de creacion?
      		$utencilio->updated_at=date('Y-m-d');
      		$utencilio->save();
      		$statusCode['mensaje']="El registro se ha Modificado de forma exitosa";
      		$error=false;
      	}
      	return response()->json(['error'=>$error,'statusCode'=>$statusCode]);
	}

	public function destroy($id){

		$separar=explode("-", $id);
		$id=$separar[0];
		$estatus=$separar[1];
 		$utencilio=Utencilio::find($id);

		if($estatus=='A'){
 		$utencilio->estatus='E';

 		$statusCode['mensaje']="El utencilio se a eliminado de forma exitosa";
		}else{
		$utencilio->estatus='A';
		$statusCode['mensaje']="El utencilio se a activado de forma exitosa";

		}
		$utencilio->save();
 		return response()->json(['statusCode'=>$statusCode]);

	}
}
