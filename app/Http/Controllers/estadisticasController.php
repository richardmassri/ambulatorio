<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
class estadisticasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    $consultaEstadisticas=DB::table('estados')->select(DB::raw('sum(case when(estados.id=24) then 1 else 0 END) as miranda,
		sum(case when(estados.id=25) then 1 else 0 END) as falcon,
		sum(case when(estados.id=26) then 1 else 0 END) as trujillo,
		sum(case when(estados.id=2) then 1 else 0 END) as DISTRITO_CAPITAL,
		sum(case when(estados.id=3) then 1 else 0 END) as AMAZONAS,
		sum(case when(estados.id=4) then 1 else 0 END) as ANZOATEGUI,
		sum(case when(estados.id=5) then 1 else 0 END) as APURE,
		sum(case when(estados.id=6) then 1 else 0 END) as ARAGUA,
		sum(case when(estados.id=7) then 1 else 0 END) as BARINAS,
		sum(case when(estados.id=8) then 1 else 0 END) as BOLIVAR,
		sum(case when(estados.id=9) then 1 else 0 END) as CARABOBO,
		sum(case when(estados.id=10) then 1 else 0 END) as COJEDES,
		sum(case when(estados.id=11) then 1 else 0 END) as DELTA_AMACURO,
		sum(case when(estados.id=12) then 1 else 0 END) as GUARICO,
		sum(case when(estados.id=13) then 1 else 0 END) as LARA,
		sum(case when(estados.id=14) then 1 else 0 END) as MERIDA,
		sum(case when(estados.id=15) then 1 else 0 END) as MONAGAS,
		sum(case when(estados.id=16) then 1 else 0 END) as NUEVA_ESPARTA,
		sum(case when(estados.id=17) then 1 else 0 END) as PORTUGUESA,
		sum(case when(estados.id=18) then 1 else 0 END) as SUCRE,
		sum(case when(estados.id=19) then 1 else 0 END) as TACHIRA,
		sum(case when(estados.id=20) then 1 else 0 END) as YARACUY,
		sum(case when(estados.id=21) then 1 else 0 END) as ZULIA,
		sum(case when(estados.id=21) then 1 else 0 END) as VARGAS,
		sum(case when(estados.id=23) then 1 else 0 END) as DEPENDENCIAS_FEDERALES'
		))->leftJoin('pacientes','pacientes.estado_id','=','estados.id')->join('atenciones','atenciones.paciente_id','=','pacientes.id')->get();
    return response()->json(['consultaEstadisticas'=>$consultaEstadisticas]);


        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
