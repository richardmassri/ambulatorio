<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Medico;
use DB;
use App\Estado;
use Session;

class medicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario=DB::table('medicos')->orderBy('nombre','asc')->get();
        //dd($usuario); die();
        echo json_encode(['listado'=>$usuario],true);die();
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        //dd($dataForm); die();
        $estado = Estado::all();
        //dd($dataForm); die();
        return response()->json(['estado'=>$estado]);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $clase='';
        //$error[]='';
        $request = json_decode(file_get_contents('php://input'), true);
        $nombre=isset($request['form']['nombre'])?$request['form']['nombre']:'';
        $cedula=isset($request['form']['cedula'])?$request['form']['cedula']:'';
        $apellido=isset($request['form']['apellido'])?$request['form']['apellido']:'';
        $telefono=isset($request['form']['telefono'])?$request['form']['telefono']:'';
        $estado=isset($request['form']['estado_id'])?$request['form']['estado_id']:'';
        $email=isset($request['form']['correo'])?$request['form']['correo']:'';
        $estatus=isset($request['form']['estatus'])?$request['form']['estatus']:'';
        if($nombre==''){
            $error['name']="El nombre no debe ser vacio";
        }else if(preg_match("/^[a-zA-Z ]*$/",$nombre)==false){
            $error['name']="El nombre debe contener solo letras";
        }elseif(strlen($nombre)<=3){
            $error['name']="El nombre debe contener al menos 4 letras";
        }
        if($apellido==''){
            $error['apellido']="El apellido no debe ser vacio";
        }elseif(preg_match("/^[a-zA-Z ]*$/",$apellido)==false){
            $error['apellido']="El apellido debe contener solo letras";
        }elseif(strlen($apellido)<=3){
            $error['apellido']="El apellido debe contener al menos 4 letras";
        }
        
        if($email==''){
            $error['correo']="El email no debe ser vacio";
        }elseif(preg_match("/^([a-zA-Z0-9_\.\-]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/",$email)==false){
        $error['correo']="Debe ingresar un correo valido";
        }
        if($cedula==''){
            $error['cedula']="La cédula no debe ser vacia";
        }elseif(preg_match("/^[a-zA-Z ]*$/",$cedula)==true){
            $error['cedula']="La cedula debe contener solo numeros";
        }
        if($estado==''){
            $error['estado']="El estado no debe ser vacio";
        }
        if($estatus==''){
            $error['estatus']="El estatus no debe ser vacio";
        }
        if($telefono==''){
            $error['telefono']="El telefono no debe ser vacio";
        }

        if(isset($error)){
            $error=$error;
            $statusCode['mensaje']=false;
            //$statusCode['exito']="error";
        }else{
            $consulta_user=Medico::where('cedula',$cedula)->get();
            //dd($request['form']); die();
            if(count($consulta_user)<=0){
            $medico= new Medico($request['form']);
            //$ip = $_SERVER['REMOTE_ADDR'];
            //$usuario->ip=$ip;
            $medico->created_at=date('Y-m-d');
            $medico->updated_at=date('Y-m-d');
            $medico->municipio_id=1;
            $medico->parroquia_id=1;
            $medico->estatus='A';
            $medico->usuario_ini_id=Session::get('usuario_id');
            $medico->usuario_act_id=Session::get('usuario_id');
            $medico->usuario_eli_id=Session::get('usuario_id');

            //$medico->foto='/public/js/upload';
            $medico->save();
            $statusCode['mensaje']="El registro se ha guardado de forma exitosa";
            $clase='success';
            $error=false;
        }else{
            $error['estatus']=false;
            //var_dump("aquii");
            $statusCode['mensaje']="El pacieente ya existe";
            $clase='warning';
        }
        }
        return response()->json(['error'=>$error,'statusCode'=>$statusCode,'clase'=>$clase]);
        
    }
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
