<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_Enfermedad extends Model
{
	protected $table = 'tipo_enfermedades';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'usuario_ini_id','usuario_act_id'
        ,'usuario_eli_id'
    ];
}
