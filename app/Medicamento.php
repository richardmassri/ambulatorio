<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicamento extends Model
{
	protected $table = 'medicamentos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'estatus','cantidad','usuario_ini_id','usuario_act_id'
        ,'usuario_eli_id'
    ];
}
