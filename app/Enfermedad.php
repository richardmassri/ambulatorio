<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enfermedad extends Model
{
	protected $table = 'enfermedades';
    protected $fillable = [
        'nombre', 'usuario_ini_id','usuario_act_id'
        ,'usuario_eli_id'
    ];
}
