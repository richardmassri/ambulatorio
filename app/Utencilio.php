<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Utencilio extends Model
{
	protected $table = 'utencilios';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'estatus', 'usuario_ini_id','usuario_act_id'
        ,'usuario_eli_id'
    ];
}
