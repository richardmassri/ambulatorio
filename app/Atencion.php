<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atencion extends Model
{
	protected $table = 'atenciones';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'observacion', 'fecha','paciente_id'
        ,'medico_id','enfermedad_id'
    ];

}
