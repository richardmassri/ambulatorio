<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
	protected $table = 'pacientes';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'apellido', 'cedula','correo','telefono', 'direccion', 'usuario_ini_id','usuario_act_id'
        ,'usuario_eli_id', 'estado_id', 'municipio_id','parroquia_id','estatus'
    ];

    public function estado_paciente() { 
    //return $this->belongsTo('App\Estado');
    return $this->belongsTo('App\Estado', 'estado_id', 'id');
}
}
