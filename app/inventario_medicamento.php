<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class inventario_medicamento extends Model
{
	protected $table = 'inventario_medicamentos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fecha','paciente_id'
        ,'medico_id','medicamento_id'
    ];}
